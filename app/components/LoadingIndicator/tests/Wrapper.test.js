import React from 'react';
import { render } from 'react-testing-library';

import Wrapper from '../Wrapper';

describe('<Wrapper />', () => {
  it('deve renderizar uma tag <div>', () => {
    const { container } = render(<Wrapper />);
    expect(container.firstElementChild.tagName).toEqual('DIV');
  });

  it('deve ter um atributo class', () => {
    const { container } = render(<Wrapper />);
    const element = container.firstElementChild;
    expect(element.hasAttribute('class')).toBe(true);
  });

  it('deve ter um atributo válido', () => {
    const id = 'test';
    const { container } = render(<Wrapper id={id} />);
    const element = container.firstElementChild;
    expect(element.id).toEqual(id);
  });

  it('não deve ter um atributo válido', () => {
    const { container } = render(<Wrapper attribute="test" />);
    const element = container.firstElementChild;
    expect(element.hasAttribute('attribute')).toBe(false);
  });
});
