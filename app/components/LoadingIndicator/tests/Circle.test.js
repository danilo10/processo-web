import React from 'react';
import { render } from 'react-testing-library';

import Circle from '../Circle';

describe('<Circle />', () => {
  it('deve renderizar uma tag <div>', () => {
    const { container } = render(<Circle />);
    expect(container.firstChild.tagName).toEqual('DIV');
  });

  it('deve ter um atributo class', () => {
    const { container } = render(<Circle />);
    expect(container.firstChild.hasAttribute('class')).toBe(true);
  });

  it('não deve ter atributos', () => {
    const id = 'test';
    const { container } = render(<Circle id={id} />);
    expect(container.firstChild.hasAttribute('id')).toBe(false);
  });
});
