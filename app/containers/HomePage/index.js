import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { Fieldset } from 'primereact/fieldset';
import { InputMask } from 'primereact/inputmask';
import { Calendar } from 'primereact/calendar';
import { Dropdown } from 'primereact/dropdown';
import { InputText } from 'primereact/inputtext';
import { Checkbox } from 'primereact/checkbox';
import { Button } from 'primereact/button';
import { Card } from 'primereact/card';
import { Paginator } from 'primereact/paginator';

const items = [
    {label: 'Em andamento', value: '0'},
    {label: 'Desmembrado', value: '1'},
    {label: 'Em recurso', value: '2'},
    {label: 'Finalizado', value: '3'},
    {label: 'Arquivado', value: '4'}
];

const processos = [
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" },
    { "npu": "a", "dd": "b", "sj": "c", "pfc": "d", "d": "e", "s": "f", "r": "g", "r": "h", "a": "i" }
];

export default class HomePage extends Component {

    state = {
        situacao: '',
        panelCollapsed: false,
        processosCollapsed: false,
        segredoJustica: false,
        processosPagina: 10,
        primeiraPagina: 0,
        processosPorPagina: []
    }

    componentDidMount() {
        this.setState({ processosPorPagina: processos.slice(this.state.primeiraPagina, this.state.processosPagina) })
    }

    render() {
        return (
            <article>
                <Helmet>
                    <title>Home Page</title>
                    <meta
                        name="Gestão de Processos"
                        content="Funcionalidade responsável pela gestão dos processos"
                    />
                </Helmet>
                <div>
                    <h1>Gestão de processos | Consultar processo</h1>
                    <div>
                        <Fieldset 
                            legend="Filtros" 
                            toggleable={true}
                            onToggle={(e) => this.setState({panelCollapsed: e.value})}
                            collapsed={this.state.panelCollapsed}
                        >
                            <div className="p-grid p-fluid p-justify-center">
                                <div className="p-col-12 p-md-2">
                                    <InputMask 
                                        id="cnj" 
                                        placeholder="N˚ Processo Unificado" 
                                        mask="9999999-99.9999.9.99.9999" 
                                        style={{ padding: 10 }}
                                    />
                                </div>
                                <div className="p-col-12 p-md-3">
                                    <Calendar 
                                        value={this.state.dataInit}
                                        onChange={(e) => this.setState({ dataInit: e.value })}
                                        showIcon={true} 
                                        placeholder="Data distribuição - Início"
                                        inputStyle={{ padding: 10 }}
                                    />
                                </div>
                                <div className="p-col-12 p-md-3">
                                    <Calendar 
                                        value={this.state.dataFim}
                                        onChange={(e) => this.setState({ dataFim: e.value })}
                                        showIcon={true} 
                                        placeholder="Data distribuição - Fim"
                                        inputStyle={{ padding: 10 }}
                                    />
                                </div>
                                <div className="p-col-12 p-md-2">
                                    <Dropdown 
                                        value={this.state.situacao} 
                                        options={items} 
                                        placeholder="Situação" 
                                        onChange={(e) => {this.setState({ situacao: e.value })}}
                                        style={{ padding: 4 }}
                                    />
                                </div>
                                <div className="p-col-12 p-md-2" style={{ marginTop: 9, alignItems: 'center', alignContent: 'center' }}>
                                    <Checkbox checked={this.state.segredoJustica} onChange={(e) => this.setState({ segredoJustica: e.checked })} />
                                    <label style={{ marginLeft: 10 }}>Segredo de Justiça</label>
                                </div>                            
                            </div>
                            <div className="p-grid p-fluid" style={{ marginTop: 10 }}>
                                <div className="p-col-12 p-md-5">
                                    <InputText
                                        placeholder="Pasta física cliente"
                                        style={{ padding: 10 }}
                                    />
                                </div>
                                <div className="p-col-12 p-md-5">
                                    <InputText
                                        placeholder="Responsável"
                                        style={{ padding: 10 }}
                                    />
                                </div>
                                <div className="p-col-12 p-md-2" style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                                    <Button label="Consultar" style={{ width: 200, height: 42 }} />
                                </div>
                            </div>
                        </Fieldset>
                    </div>
                </div>
                <div>
                    <Fieldset 
                            legend="Processos" 
                            toggleable={true}
                            onToggle={(e) => this.setState({processosCollapsed: e.value})}
                            collapsed={this.state.processosCollapsed}
                            style={{ marginTop: 20, marginBottom: 10 }}
                    >
                        <div style={{ marginTop: 5 }}>
                            {this.state.processosPorPagina.map((processo, index) => {
                                return (
                                    <Card key={index} style={{ marginBottom: 10 }} title="7232277-88.8888.8.89.9999" subTitle="Responsável: Danilo Raulino de Liz - João da Silva">
                                        {index}
                                    </Card>
                                )
                            })}
                        </div>
                        <div>
                            <Paginator 
                                style={{ padding: 5 }}
                                rowsPerPageOptions={[10,20,30]} 
                                first={this.state.primeiraPagina} 
                                totalRecords={processos.length}
                                rows={this.state.processosPagina}
                                onPageChange={(e) => this.setState({ primeiraPagina: e.first, processosPagina: e.rows, processosPorPagina: processos.slice(e.first, e.first + e.rows) })} ></Paginator>
                        </div>
                    </Fieldset>
                </div>
            </article>
        )
    }
} 