import React from 'react';
import { Helmet } from 'react-helmet';

export function HomePage() {
    return (
        <article>
            <Helmet>
                <title>Not Found</title>
                <meta
                    name="Not Found"
                    content="Sistema fora do ar, status 404"
                />
            </Helmet>
            <div>
                <h1>Not Found</h1>
            </div>
        </article>
    )
} 