const addLocaleData = require('react-intl').addLocaleData; //eslint-disable-line
const enLocaleData = require('react-intl/locale-data/en');
const deLocaleData = require('react-intl/locale-data/de');
const ptLocaleData = require('react-intl/locale-data/br');

const brTranslationMessages = require('./translations/br.json');
const enTranslationMessages = require('./translations/en.json');
const esTranslationMessages = require('./translations/es.json');

const DEFAULT_LOCALE = 'en';

const appLocales = [
    'br',
    'en',
    'es',
];

const formatTranslationMessages = (locale, messages) => {
    const defaultFormattedMessages =
      locale !== DEFAULT_LOCALE
        ? formatTranslationMessages(DEFAULT_LOCALE, enTranslationMessages)
        : {};
    const flattenFormattedMessages = (formattedMessages, key) => {
      const formattedMessage =
        !messages[key] && locale !== DEFAULT_LOCALE
          ? defaultFormattedMessages[key]
          : messages[key];
      return Object.assign(formattedMessages, { [key]: formattedMessage });
    };
    return Object.keys(messages).reduce(flattenFormattedMessages, {});
};

const translationMessages = {
    br: formatTranslationMessages('br', brTranslationMessages),
    en: formatTranslationMessages('en', enTranslationMessages),
    es: formatTranslationMessages('es', esTranslationMessages),
};

exports.appLocales = appLocales;
exports.formatTranslationMessages = formatTranslationMessages;
exports.translationMessages = translationMessages;
exports.DEFAULT_LOCALE = DEFAULT_LOCALE;
