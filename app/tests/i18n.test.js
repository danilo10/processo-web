import { formatTranslationMessages } from '../i18n';

jest.mock('../translations/en.json', () => ({
  message1: 'standard message',
  message2: 'standard message 2',
}));

const esTranslationMessages = {
  message1: 'mensaje predeterminado',
  message2: '',
};

const brTranslationMessages = {
  message1: 'mensagem padrão',
  message2: 'mensagem padrão 2',
};

describe('formatTranslationMessages', () => {
  it('Quando não estiver configurado o lacale, aceita o DEFAULT_LOCALE', () => {
    const result = formatTranslationMessages('en', { a: 'a' });
  
    expect(result).toEqual({ a: 'a' });
  });
  
  it('Quando selecionar o locale em Espanhol, esse deve ser o o resultado', () => {
    const result = formatTranslationMessages('', esTranslationMessages);
  
    expect(result).toEqual({
      message1: 'mensaje predeterminado',
      message2: 'standard message 2'
    });
  });
  
  it('Quando selecionar o locale em Português, esse deve ser o o resultado', () => {
    const result = formatTranslationMessages('', brTranslationMessages);

    expect(result).toEqual({
      message1: 'mensagem padrão',
      message2: 'mensagem padrão 2'
    });
  });
});