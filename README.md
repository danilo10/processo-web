<h1 align="center">
  <a href="">
    Processos WEB
  </a>
</h1>

<p align="center">
  <strong>Descrição do projeto:</strong><br>
  Aplicação web para cadastro de processos
</p>

## Tópicos

- [Estrutura do projeto](#estrutura-do-projeto)
- [Configurando o projeto](#configurando-o-projeto)

## Estrutura do projeto

- `/app` - Código do projeto;
  - `/components` - Componentes compartilhados entre as views do projeto;
  - `/config` - Constantes e dados gerais (imagens, styles, tenas e etc...);
  - `/containers` - Agrupador de funcionalidades;
    - `feature1`- Funcionalidade 1;
      - `reducer` - Reducer associado a feature (um ou vários);
      - `sagas` - Sagas são relacionadas a uma feature especifica;
      - `selectors` - Selectors associados a feature;
      - `components` - Componentes associados a feature;
  - `/images` - Imagens do projeto;
  - `/tests` - Testes dos arquivos raiz;
  - `/translations` - Arquivos json de tradução (br.json, en.json e es.json);
  - `/utils` - Todos os utilitários do projeto;
  - `app.js` - Arquivo js inicial do projeto;
  - `configureStore.js` - Arquivo de configuração do redux, fluter e redux-saga do projeto;
  - `global-styles.js` - Alguns padrões de estilo de algumas tags html;
  - `i18n.js` - Arquivo de configuração de trdução do projeto;
  - `index.html` - HTML inicial do projeto;
  - `reducers.js` - Arquivo central dos reducers do projeto;
  - `serviceWorker.js` - Arquivo para registrar os services worker do projeto;
- `/internals` - Babel Webpack dev e prod;
- `/server` - Servidor Webpack dev e prod;
- `.editorconfig` - Configurações com padrões para várias IDEs;
- `.eslintrc.js` - Configurações do lint de código;
- `.stylelintrc` - Linter para ajudar a evitar erros de CSS nos estilos de component;
- `babel.config.js` - Configurações do babel;
- `package.json` - npms do projeto;
- `README.md` - Documentação técnica do projeto.

## Configurando o projeto

# Dev

Após o clone do projeto, vá até a pasta raiz de rode `npm install`. Após essa etapa basta digitar `npm start` e o projeto vai rodar no endereço `http://localhsot:3000`.

# Prod

rode o comando `npm run start:production` ele irá passar por todos os passos de teste, build e irá iniciar o build no endereço `http://localhost:3000`.

# Teste

rode o comando `npm run test` e irá rodar os testes do projeto e no final apresentar o grafico com a porcentagem de cobertura de código.